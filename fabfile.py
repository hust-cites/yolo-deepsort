from fabric.api import *

env.hosts = []
env.key_filename = '~/.ssh/id_rsa'
env.user = 'zdeploy'

dev_schema = '/data/{}/personal_projects/multi-object-trackings/deepsort-human-tracking/'
users_has_permission = 'anhlt11'


def sync_to_server(username=None, dev=True, host='10.40.19.19'):
    """
    Rsync jar to server
    """
    if username not in users_has_permission and dev:
        print("User {0} do not have permissions".format(username))
        return

    path = dev_schema.format(username)
    command = "rsync -avh --progress --delete --exclude .git --exclude *.pyc --exclude __pycache__ --exclude .idea  --exclude eco_features . zdeploy@{0}:{1}".format(
        host, path)
    local(command)


def deploy(username, host='10.40.19.19'):
    """
    Deploy to server development
    """

    if username not in users_has_permission:
        print("User {0} do not have permissions".format(username))
        return

    print("Update to development....")
    sync_to_server(username, dev=True, host=host)